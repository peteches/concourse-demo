#!/bin/sh -x
apk update
apk add git

git clone peteches-concourse-demo peteches-concourse-demo-output

cd peteches-concourse-demo-output
date > bumpme

git config --global user.email "nobody@concourse-ci.org"
git config --global user.name "Concourse"

git add .
git commit -m "Bumped date"
